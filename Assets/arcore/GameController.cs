﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class GameController : MonoBehaviour
{
	public static GameController instance;
	[Header ("BrachTreeLists")]
	public List<GameObject> objfiles;
	public List<GameObject> ButtonList;
	public List<string> objnames;
	public List<GameObject> translateTargets;
	public List<GameObject> rotateTargets;
	public List<GameObject> visibleObjects;
	public List<GameObject> scaleObject;
	//[Header("Canvas_Tree")]
	public GameObject btn_TreeLoad, uniCan;
	public  GameObject trButton, rotButton, prMeasure, canbut;
	private bool toggle = false;
	private bool trToggle = false;
	private bool rotToggle = false;
	public GameObject trGizmo;
	public GameObject rotGizmo;
	public bool isMoveable = false;

	void Start ()
	{
		instance = this;
		for (int i = 0; i < GameManager1.instance.objfiles.Count; i++) {
			objfiles.Add (GameManager1.instance.objfiles [i]);
			objnames.Add (GameManager1.instance.objfiles [i].name);

		}
	}

	void Update()
	{
		Debug.Log (visibleObjects.Count);
	}

	public void OnClickTreeLoad ()
	{
		toggle = !toggle;

		btn_TreeLoad.SetActive (toggle);
		uniCan.SetActive (true);

		AttachObjectList.instance.btn_Reset.GetComponent<Image> ().raycastTarget = true;
		AttachObjectList.instance.btn_Precise.GetComponent<Image> ().raycastTarget = true;
	}

	public void Reset ()
	{
		int count = ButtonList.Count;
		for (int i = 0; i < count; i++) {
			
			Destroy (ButtonList [i]);
			Destroy (objfiles [i]);


		}
		ButtonList.Clear ();
		GameManager1.instance.objfiles.Clear ();
		GameManager1.info.Clear ();
		GameManager1.instance.objnames.Clear ();
		objfiles.Clear ();
		objnames.Clear ();
	}

	public void BackButton ()
	{

		SceneManager.LoadScene ("Scene1");
	}

	public void Close ()
	{
		Application.Quit ();
	}


	public void TrGizmo ()
	{
		trToggle = !trToggle;
		rotButton.GetComponent<Button> ().enabled = !trToggle;

		trGizmo.SetActive (trToggle);

	}


	public void RotGizmo ()
	{
		rotToggle = !rotToggle;
		trButton.GetComponent<Button> ().enabled = !rotToggle;
		//GetComponent<Toggle> ().onValueChanged
		rotGizmo.SetActive (rotToggle);
	
	}
}
