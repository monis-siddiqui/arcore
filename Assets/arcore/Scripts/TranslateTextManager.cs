﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;


public class  TranslateTextManager : MonoBehaviour
{


	private InputField fName;
	//public Text fText;
	float num;

	void Start()
	{
		fName = GetComponent<InputField> ();

	}

	public void SetFieldXPos()
	{
		if (GameController.instance.isMoveable) {
		num = float.Parse (fName.text);
			foreach (GameObject g in GameController.instance.visibleObjects) {
				g.transform.DOMove (new Vector3 (num*0.001f, g.transform.position.y, g.transform.position.z), 0);
			}
		}
	}

	public void SetFieldYPos ()
	{
		if (GameController.instance.isMoveable) {
		num = float.Parse(fName.text);
			foreach (GameObject g in GameController.instance.visibleObjects) {
				g.transform.DOMove (new Vector3 (g.transform.position.x, num*0.001f, g.transform.position.z), 0f);
			}
		}
	}  


	public void SetFieldZPos()
	{
		if (GameController.instance.isMoveable) {
		num = float.Parse(fName.text);
			foreach (GameObject g in GameController.instance.visibleObjects) {
				g.transform.DOMove (new Vector3 (g.transform.position.x, g.transform.position.y, num*0.001f), 0f);
			}
		}
		}


}