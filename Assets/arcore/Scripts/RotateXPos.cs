﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class RotateXPos : MonoBehaviour,  IPointerClickHandler
{
	public static float rotXPos;
	public InputField inputField;
	float n = 5;

	void Start ()
	{

	}

	public void OnPointerClick (PointerEventData pointerEventData)
	{
		if (GameController.instance.isMoveable) {
			foreach (GameObject g in GameController.instance.visibleObjects) {
				rotXPos = rotXPos + n;
				//g.transform.localEulerAngles = new Vector3 (rotXPos, g.transform.localRotation.y, g.transform.localRotation.z);
				g.transform.localRotation = Quaternion.Euler( new Vector3 (rotXPos, g.transform.localRotation.eulerAngles.y, g.transform.localRotation.eulerAngles.z));
				inputField.text = (rotXPos).ToString ();
			}
		}
	}


}
