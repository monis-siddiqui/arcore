﻿           using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RotateObjectScript1 : MonoBehaviour
{

	public GameObject _Cube;
	public InputField inputField;
	float n = 5;

	void Start ()
	{

	}

	public void RotateX (int dir)
	{
		_Cube.transform.localEulerAngles = new Vector3 (_Cube.transform.localEulerAngles.x + n * dir, _Cube.transform.localEulerAngles.y, _Cube.transform.localEulerAngles.z);
		inputField.text = (_Cube.transform.localEulerAngles.x).ToString ();

	}


	public void RotateY (int dir)
	{
		_Cube.transform.localEulerAngles = new Vector3 (_Cube.transform.localEulerAngles.x, _Cube.transform.localEulerAngles.y + n * dir, _Cube.transform.localEulerAngles.z);
		inputField.text = (_Cube.transform.localEulerAngles.y).ToString ();
	}

	public void RotateZ (int dir)
	{
		_Cube.transform.localEulerAngles = new Vector3 (_Cube.transform.localEulerAngles.x, _Cube.transform.localEulerAngles.y, _Cube.transform.localEulerAngles.z + n * dir);
		inputField.text = (_Cube.transform.localEulerAngles.z).ToString ();
	}





}
