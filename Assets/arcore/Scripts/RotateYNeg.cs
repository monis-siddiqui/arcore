﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class RotateYNeg : MonoBehaviour ,  IPointerClickHandler
{
	public static float rotYNeg;
	public InputField inputField;
	float n = 5;

	void Start ()
	{

	}

	public void OnPointerClick (PointerEventData pointerEventData)
	{
		if (GameController.instance.isMoveable) {
			foreach (GameObject g in GameController.instance.visibleObjects) {
				rotYNeg = rotYNeg - n;
				//g.transform.localEulerAngles = new Vector3 (g.transform.localRotation.x, rotYNeg , g.transform.localRotation.z);
				g.transform.localRotation = Quaternion.Euler (new Vector3 (g.transform.localRotation.eulerAngles.x, rotYNeg, g.transform.localRotation.eulerAngles.z));
				inputField.text = (rotYNeg).ToString ();
			}
		}
	}


}


