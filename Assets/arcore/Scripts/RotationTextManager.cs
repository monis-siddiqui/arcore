﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class RotationTextManager : MonoBehaviour
{


	public InputField fName;
	public RotaitonValues rotationValues;

	float num;
	float rot;

	void Start ()
	{
		fName = GetComponent<InputField> ();
	}

	public void SetFieldX ()
	{
		if (GameController.instance.isMoveable) {
			if (fName.text == "") {
				print ("Null");
				return;
			}
			if (fName.text == "-" && fName.text.Length == 1)
				fName.text = "-";
			else
			num = float.Parse (fName.text);
			foreach (GameObject g in GameController.instance.visibleObjects) {
				Debug.Log ("BEFORE :" +g.transform.localRotation.eulerAngles);
				g.transform.localRotation = Quaternion.Euler( new Vector3 (num, g.transform.localRotation.eulerAngles.y, g.transform.localRotation.eulerAngles.z));
				//g.transform.rotation = Quaternion.Euler(new Vector3(num,g.transform.rotation.y,g.transform.rotation.z));
				Debug.Log ("AFTER :" +g.transform.localRotation.eulerAngles);
				//RotateXPos.rotXPos = num;
				//RotateXNeg.rotXNeg = num;
			}
		}

	}

	public void SetFieldY ()
	{
		if (GameController.instance.isMoveable) {
			if (fName.text == "") {
				print ("Null");
				return;
			}
			if (fName.text == "-" && fName.text.Length == 1)
				fName.text = "-";
			else
			num = int.Parse (fName.text);
			foreach (GameObject g in GameController.instance.visibleObjects) {
				
				//g.transform.localEulerAngles = new Vector3 (transform.localRotation.x, (g.transform.localRotation.y + num), g.transform.localRotation.z);
				g.transform.localRotation = Quaternion.Euler( new Vector3 (g.transform.localRotation.eulerAngles.x, num, g.transform.localRotation.eulerAngles.z));
				//RotateYPos.rotYPos = num;
				//RotateYNeg.rotYNeg = num;
			}
		}
	}


	public void SetFieldZ ()
	{
		if (GameController.instance.isMoveable) {
			if (fName.text == "") {
				print ("Null");
				return;
			}
			if (fName.text == "-" && fName.text.Length == 1)
				fName.text = "-";
			else
			num = int.Parse (fName.text);
			foreach (GameObject g in GameController.instance.visibleObjects) {
				//g.transform.localEulerAngles = new Vector3 (g.transform.localRotation.x, g.transform.localRotation.y, (g.transform.localRotation.z + num));
				g.transform.localRotation = Quaternion.Euler( new Vector3 (g.transform.localRotation.eulerAngles.x, g.transform.localRotation.eulerAngles.y, num));
				//RotateZPos.rotZPos = num;
				//RotateZNeg.rotZNeg = num;

			}
		}
	}
}