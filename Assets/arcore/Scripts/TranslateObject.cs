﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TranslateObject : MonoBehaviour
{

	public InputField _inputField;

	public GameObject obj;
	float num;

	void Start ()
	{
		
	}

	public void OnClick_X_Axis ()
	{
		obj.transform.position = new Vector3 (obj.transform.position.x + 1f, obj.transform.position.y, obj.transform.position.z);
		_inputField.text = (obj.transform.position.x).ToString ();
	}

	public void OnClick_Y_Axis ()
	{
		obj.transform.position = new Vector3 (obj.transform.position.x, obj.transform.position.y + 1f, obj.transform.position.z);
		_inputField.text = (obj.transform.position.y).ToString ();
	}

	public void OnClick_Z_Axis ()
	{
		obj.transform.position = new Vector3 (obj.transform.position.x, obj.transform.position.y, obj.transform.position.z + 1f);
		_inputField.text = (obj.transform.position.z).ToString ();
	}





	public void OnClick_X_AxisNegative ()
	{
		obj.transform.position = new Vector3 (obj.transform.position.x - 1f, obj.transform.position.y, obj.transform.position.z);	
		_inputField.text = (obj.transform.position.x).ToString ();
	}

	public void OnClick_Y_AxisNegative ()
	{
		obj.transform.position = new Vector3 (obj.transform.position.x, obj.transform.position.y - 1f, obj.transform.position.z);
		_inputField.text = (obj.transform.position.y).ToString ();
	}

	public void OnClick_Z_AxisNegative ()
	{
		obj.transform.position = new Vector3 (obj.transform.position.x, obj.transform.position.y, obj.transform.position.z - 1f);
		_inputField.text = (obj.transform.position.z).ToString ();
	}

}
