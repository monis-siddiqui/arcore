﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class RotateZNeg : MonoBehaviour,  IPointerClickHandler
{
	public static float rotZNeg;
	public InputField inputField;
	float n = 5;

	void Start ()
	{

	}

	public void OnPointerClick (PointerEventData pointerEventData)
	{
		if (GameController.instance.isMoveable) {
			foreach (GameObject g in GameController.instance.visibleObjects) {
				rotZNeg = rotZNeg - n;
				//g.transform.localEulerAngles = new Vector3 (g.transform.localRotation.x, g.transform.localRotation.y, rotZNeg);
				g.transform.localRotation = Quaternion.Euler (new Vector3 (g.transform.localRotation.eulerAngles.x, g.transform.localRotation.eulerAngles.y, rotZNeg));
				inputField.text = (rotZNeg).ToString ();
			}
		}
	}

}

