﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
public class TranslateYPos : MonoBehaviour ,  IPointerClickHandler
{
	
	public InputField inputField;
	float n = 5;
	bool isBool;
	void Start () {

	}

	public void OnPointerClick(PointerEventData pointerEventData)
	{
		if (GameController.instance.isMoveable) {
			foreach (GameObject g in GameController.instance.visibleObjects) {
				if (inputField.text == "")
					inputField.text = (g.transform.position.y/0.001f).ToString();
				float num = float.Parse (inputField.text) + 1f;
				g.transform.position = new Vector3 (g.transform.position.x, num, g.transform.position.z);
				inputField.text = num.ToString ();
			}
		}
	}
}
