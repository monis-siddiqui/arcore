﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class RotateXNeg : MonoBehaviour,  IPointerClickHandler
{
	public static float rotXNeg;
	public InputField inputField;
	float n = 5;

	void Start ()
	{

	}

	public void OnPointerClick (PointerEventData pointerEventData)
	{
		if (GameController.instance.isMoveable) {
			foreach (GameObject g in GameController.instance.visibleObjects) {
				rotXNeg = rotXNeg - n;
				//g.transform.localEulerAngles = new Vector3 (rotXNeg, g.transform.localEulerAngles.y, g.transform.localEulerAngles.z);
				g.transform.localRotation = Quaternion.Euler (new Vector3 (rotXNeg, g.transform.localRotation.eulerAngles.y, g.transform.localRotation.eulerAngles.z));
				inputField.text = (rotXNeg).ToString ();
	
			}
		}
	}
	

}
