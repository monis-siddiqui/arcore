﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
public class RotateZPos : MonoBehaviour,  IPointerClickHandler
{
	public static float rotZPos;
	public InputField inputField;
	float n = 5;

	void Start () {

	}

	public void OnPointerClick(PointerEventData pointerEventData)
	{
		if (GameController.instance.isMoveable) {
			foreach (GameObject g in GameController.instance.visibleObjects) {
				rotZPos = rotZPos + n;
				//g.transform.localEulerAngles = new Vector3 (g.transform.localRotation.x, g.transform.localRotation.y, rotZPos);
				g.transform.localRotation = Quaternion.Euler (new Vector3 (g.transform.localRotation.eulerAngles.x, g.transform.localRotation.eulerAngles.y, rotZPos));
				inputField.text = (rotZPos).ToString ();
			}
		}
	}

}

