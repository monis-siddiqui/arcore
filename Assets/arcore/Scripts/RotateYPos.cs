﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class RotateYPos : MonoBehaviour ,  IPointerClickHandler
{
	public static float rotYPos;
	public InputField inputField;
	float n = 5;

	void Start ()
	{

	}

	public void OnPointerClick (PointerEventData pointerEventData)
	{
		if (GameController.instance.isMoveable) {
			foreach (GameObject g in GameController.instance.visibleObjects) {
				rotYPos = rotYPos + n;
				//g.transform.localEulerAngles = new Vector3 (g.transform.localRotation.x, rotYPos, g.transform.localRotation.z);
				g.transform.localRotation = Quaternion.Euler (new Vector3 (g.transform.localRotation.eulerAngles.x, rotYPos, g.transform.localRotation.eulerAngles.z));
				inputField.text = (rotYPos).ToString ();
			}
		}
	}
}
