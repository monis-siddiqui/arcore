﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ObjectScript : MonoBehaviour
{
	public GameObject button;
	public GameObject can1;
	int count;
	public InputField _InPutField;

	void Start ()
	{
		//OnClick ();
		SetField ();
	}

	public void SetField ()
	{
		count = GameManager1.instance.objfiles.Count;
		Debug.Log (count);
		OnClick ();
	}

	public void OnClick ()
	{
		for (int i = 0; i < count; i++) 
		{
			GameObject temp=	Instantiate (button, can1.transform);
			GameController.instance.ButtonList.Add (temp);
			temp.transform.GetChild (0).GetComponent<Text> ().text = GameController.instance.objnames [i];
		}
	}

	public void OnClickFolderLoad(){
	
		SceneManager.LoadScene ("folderload");
	}

}
