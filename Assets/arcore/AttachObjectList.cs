﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class AttachObjectList : MonoBehaviour
{
	public static AttachObjectList instance;
	public GameObject backToMenu, button_Detail, preciseMeasure, selectOneObjMsg, btn_Precise, btn_Reset;

	void Awake ()
	{
		instance = this;
	}


	void Start ()
	{
		if (GameManager1.instance.objfiles.Count < 1) {
			backToMenu.SetActive (true);
			button_Detail.SetActive (false);
		} else {
			button_Detail.SetActive (true);
		}
	}

	public void LoadMainMenu ()
	{
		backToMenu.SetActive (false);
		SceneManager.LoadScene ("Scene1");
	}

	public void ClickPreciseMeasurement ()
	{
		if (GameController.instance.visibleObjects.Count != 1) {
			selectOneObjMsg.SetActive (true);
			Invoke ("DisableMsg", 1);
			
		} else {
			selectOneObjMsg.SetActive (false);
			if(preciseMeasure.activeSelf==false)
			preciseMeasure.SetActive (true);
			else
				preciseMeasure.SetActive (false);
		}
	}

	void DisableMsg ()
	{
		selectOneObjMsg.SetActive (false);
	}

}
