﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Linq;

public class addScript : MonoBehaviour 
{

    public static string filepath ;
    int count;
	int polygonCount;
    public static GameObject alpha;
	public static GameObject newObj;
	//public static List<string> name;
   // public static string gameobjname;
	//public static List<GameObject> listObjects;
	public Text GameObjectCount;
	public Text PolygonCount;
	public GameObject canvas;
	public List<GameObject> objectlist_updated;
	//[HideInInspector]
	public List<string> objName;
	public BoxCollider m_Coll;
    void Start()
    {
        count = 1;
		int counter = 0;
	
		for (int k = 0;k< GameManager1.instance.objfiles.Count; k++) {

			if (!objName.Contains (GameManager1.instance.objfiles [k].name)) {

				objName.Add (GameManager1.instance.objfiles [k].name);
				objectlist_updated.Add (GameManager1.instance.objfiles [k]);
				GameObject child1 = GameManager1.instance.objfiles [k].transform.GetChild(0).gameObject;
				child1.AddComponent<MeshCollider>();
//				GameObject child2 = GameManager1.instance.objfiles [k].transform.GetChild(1).gameObject;
//				child2.AddComponent<MeshCollider>();
//				GameObject child3 = GameManager1.instance.objfiles [k].transform.GetChild(2).gameObject;
//				child3.AddComponent<MeshCollider>();
//				GameManager1.instance.objfiles [k].AddComponent<MeshCollider>();
//				m_Coll = GameManager1.instance.objfiles [k].GetComponent<BoxCollider>();
//				m_Coll.size = new Vector3 (5, 5, 5);
//				m_Coll.center = new Vector3 (0, 2.5f, 0);
			} else
				GameManager1.instance.objfiles.Remove (GameManager1.instance.objfiles [k]);
		}

    }
	

	void Update ()
    {

		GameObjectCount.text= "Number of selected files are : " + objectlist_updated.Count;
		PolygonCount.text = "Total polygon number of loaded objects : "+ countPolygons();

    }

	public int countPolygons()
	{
		int count=0;
		foreach (GameObject g in objectlist_updated) {
			MeshFilter mfilter =g.GetComponent<MeshFilter> ();
			if (mfilter != null) {
				count+=mfilter.sharedMesh.triangles.Length/3;
			}
			MeshFilter[] meshFilters = g.GetComponentsInChildren<MeshFilter>();
			if (meshFilters != null)
				foreach (MeshFilter meshfilter in meshFilters) {
					count += meshfilter.sharedMesh.triangles.Length/3;
				}
		}
		return count;
	}

    public void onbtnclk()
    {
//		foreach (var item in listObjects) 
//		{
//			DontDestroyOnLoad(item);
//			name.Add (item.name);
//		}
        SceneManager.LoadScene("HelloAR");
        
    }

	public void OnFileClick(){
		SceneManager.LoadScene ("fileload");


	}

	public void OnFolderClick(){

		SceneManager.LoadScene ("folderload");

	}
}
