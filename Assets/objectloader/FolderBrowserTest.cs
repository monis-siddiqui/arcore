﻿using UnityEngine;
using System.Collections;
using SimpleFileBrowser;
using System.IO;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class FolderBrowserTest : MonoBehaviour
{

	List<GameObject> objs = new List<GameObject> ();
	// Warning: paths returned by FileBrowser dialogs do not contain a trailing '\' character
	// Warning: FileBrowser can only show 1 dialog at a time
	List<FileInfo> fo = new List<FileInfo> ();
	public static FolderBrowserTest instance;

	void Awake ()
	{
//		if(instance == null)
//		{
//
//			instance = this;
//			DontDestroyOnLoad(gameObject);
//		}else if(instance != this)
//		{
//			Destroy(gameObject);
//		}

	}

	void Start ()
	{
		// Set filters (optional)
		// It is sufficient to set the filters just once (instead of each time before showing the file browser dialog), 
		// if all the dialogs will be using the same filters
		FileBrowser.SetFilters (true, new FileBrowser.Filter ("obj", ".obj"));

		// Set default filter that is selected when the dialog is shown (optional)
		// Returns true if the default filter is set successfully
		// In this case, set Images filter as the default filter
		//FileBrowser.SetDefaultFilter( ".obj" );

		// Set excluded file extensions (optional) (by default, .lnk and .tmp extensions are excluded)
		// Note that when you use this function, .lnk and .tmp extensions will no longer be
		// excluded unless you explicitly add them as parameters to the function
		//FileBrowser.SetExcludedExtensions( ".lnk", ".tmp", ".zip", ".rar", ".exe" , ".jpg", ".png" ,".txt", ".pdf");


		// Add a new quick link to the browser (optional) (returns true if quick link is added successfully)
		// It is sufficient to add a quick link just once
		// Name: Users
		// Path: C:\Users
		// Icon: default (folder icon)
		FileBrowser.AddQuickLink ("Users", "C:\\Users", null);

		// Show a save file dialog 
		// onSuccess event: not registered (which means this dialog is pretty useless)
		// onCancel event: not registered
		// Save file/folder: file, Initial path: "C:\", Title: "Save As", submit button text: "Save"
		// FileBrowser.ShowSaveDialog( null, null, false, "C:\\", "Save As", "Save" );

		// Show a select folder dialog 
		// onSuccess event: print the selected folder's path
		// onCancel event: print "Canceled"
		// Load file/folder: folder, Initial path: default (Documents), Title: "Select Folder", submit button text: "Select"
		FileBrowser.ShowLoadDialog ((path) => {
			Debug.Log ("Selected: " + path);
			StartCoroutine (ShowLoadDialogCoroutine ());
		}, 
			() => {
				Debug.Log ("Canceled");
			}, 
			true, null, "Select Folder", "Select");

		// Coroutine example
		//StartCoroutine( ShowLoadDialogCoroutine() );
	}

	IEnumerator ShowLoadDialogCoroutine ()
	{
		print ("Here");
		// Show a load file dialog and wait for a response from user
		// Load file/folder: file, Initial path: default (Documents), Title: "Load File", submit button text: "Load"
		//try{
		//	yield return FileBrowser.WaitForLoadDialog( true, null, "Load File", "Load" );
		//} catch(UnityException e){
		//Debug.Log (e.Message);
		//}
		print ("Here1");
		// Dialog is closed
		// Print whether a file is chosen (FileBrowser.Success)
		// and the path to the selected file (FileBrowser.Result) (null, if FileBrowser.Success is false)
		Debug.Log (FileBrowser.Success + " " + FileBrowser.Result);

		List<FileInfo> ss = DirSearch ("" + FileBrowser.Result);

		int j = 2;
		foreach (FileInfo temp in ss) {

			//print ("tempname" + temp.FullName);
			GameObject go = new GameObject ();

			go.name = "Object" + j;
			print ("1" + temp);
			#if !UNITY_EDITOR && UNITY_ANDROID
			go = OBJLoader.LoadOBJFile ("" +FileBrowser.Result+"/"+ temp);
			#endif
			#if UNITY_EDITOR
			go = OBJLoader.LoadOBJFile ("" + temp);
			#endif
			print ("2go" + go);
			objs.Add (go);
			if (!GameManager1.instance.objfiles.Contains (go)) {
				GameManager1.info.Add (temp);
				GameManager1.instance.objnames.Add (go.name);
				GameManager1.instance.objfiles.Add (go);
			}
			go.transform.position = new Vector3 (2, 2, 2) * j;
			go.SetActive (false);
			//	go.SetActive (false);
		

	     
			j++;

		}

		foreach (GameObject temp in objs) {
			DontDestroyOnLoad (temp);
		
		}

		Debug.Log ("FileCounts: " + ss.Count);
		SceneManager.LoadScene ("Scene1", LoadSceneMode.Single);
		yield return null;
	}



	private List<FileInfo> DirSearch (string sDir)
	{
		try {
			//	Debug.Log("Try");
			DirectoryInfo dir = new DirectoryInfo (sDir);
			FileInfo[] info = dir.GetFiles ("*.obj");
			//info[0].
//			if (info.Length == 1&&!info[0].Name.StartsWith("._"))
//			{
//				var fileName = info[0].Name;
//			    var	filePath = sDir;
//				return;
//			}

			foreach (var temp in info) {

				var fileName = temp.Name;
				var	filePath = sDir;
				fo.Add (temp);
				Debug.Log (temp.FullName);
				Debug.Log (temp);
			}
			return fo;
//			foreach (string d in Directory.GetDirectories(sDir))
//			{
//				DirSearch(d);
//
//			}
		} catch (System.Exception excpt) {
			//Debug.Log ("Error");
			//	StartCoroutine(PlacementMethods.instance.ObjectActiveHandler(PlacementMethods.instance.objFileNotFound, 2f));
			return fo;
		}
	}
}