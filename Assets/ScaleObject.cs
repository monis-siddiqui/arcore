﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using Lean.Touch;

public class ScaleObject : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public InputField inputField;

	public void onChangeValue()
	{
			foreach (GameObject g in GameController.instance.visibleObjects) {
				if(g.GetComponent<LeanScale> ()!=null) {
					float scale = float.Parse (inputField.text);
					g.transform.localScale = new Vector3 (scale, scale, scale);
				}
		}
	}
}
