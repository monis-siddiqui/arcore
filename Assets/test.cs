﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class test : MonoBehaviour
{

	public GameObject _Cube;
	float n = 5;
	float increamentVal;
	public InputField fName;
	float num;

	void Start ()
	{
		increamentVal = _Cube.transform.localRotation.x;

	}

	public void PositiveRotateX ()
	{


		increamentVal = (increamentVal + n);
		_Cube.transform.localEulerAngles = new Vector3 (increamentVal, _Cube.transform.localRotation.y, _Cube.transform.localRotation.z);
		fName.text = increamentVal.ToString ();


	}


	public void NegativeRotateX ()
	{
		Debug.Log (increamentVal);
		increamentVal = increamentVal - n;
		_Cube.transform.localEulerAngles = new Vector3 (increamentVal, _Cube.transform.localRotation.y, _Cube.transform.localRotation.z);
		fName.text = increamentVal.ToString ();


	}

	public void SetFieldX ()
	{
		if (fName.text == "") {
			print ("Null");
			return;
		}
		if (fName.text == "-" && fName.text.Length == 1)
			fName.text = "-";
		else
			num = float.Parse (fName.text);
	
		_Cube.transform.localEulerAngles = new Vector3 (num, _Cube.transform.localRotation.y, _Cube.transform.localRotation.z);
		increamentVal = num;
	
	}




}
