﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Lean.Touch;

public class togglescript : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void OnValueChange (){
		//print (gameObject.transform.parent.GetChild (0).GetComponent<Text> ().text);
		if (gameObject.GetComponent<Toggle> ().isOn == true) {
			int index = GameController.instance.objnames.IndexOf ("" + gameObject.transform.parent.GetChild (0).GetComponent<Text> ().text);
			GameController.instance.rotateTargets.Add (GameController.instance.objfiles [index]);
			GameController.instance.translateTargets.Add (GameController.instance.objfiles [index]);
			gameObject.transform.GetChild (1).GetComponent<Text> ().text = "Activated";
			GameController.instance.isMoveable = true;
		} else {
		
			int index = GameController.instance.objnames.IndexOf ("" + gameObject.transform.parent.GetChild (0).GetComponent<Text> ().text);
			GameController.instance.rotateTargets.Remove (GameController.instance.objfiles [index]);
			GameController.instance.translateTargets.Remove (GameController.instance.objfiles [index]);
			gameObject.transform.GetChild (1).GetComponent<Text> ().text = "Dectivated";
			GameController.instance.isMoveable = false;
		}
	}

	public void OnValueChangeVisibility(){
	
		if (gameObject.GetComponent<Toggle> ().isOn == true) {
			int index = GameController.instance.objnames.IndexOf ("" + gameObject.transform.parent.GetChild (0).GetComponent<Text> ().text);
			GameController.instance.visibleObjects.Add (GameController.instance.objfiles [index]);
			gameObject.transform.GetChild (1).GetComponent<Text> ().text = "Activated";
			GameController.instance.objfiles [index].SetActive (true);
		} else {

			int index = GameController.instance.objnames.IndexOf ("" + gameObject.transform.parent.GetChild (0).GetComponent<Text> ().text);
			GameController.instance.visibleObjects.Remove (GameController.instance.objfiles [index]);
			gameObject.transform.GetChild (1).GetComponent<Text> ().text = "Dectivated";
			GameController.instance.objfiles [index].SetActive (false);
		}
	
	}

	public void OnToggleBtn()
	{
		if (gameObject.GetComponent<Toggle> ().isOn == true) {
			gameObject.transform.GetChild (1).GetComponent<Text> ().text = "Activated";
		} else {
			gameObject.transform.GetChild (1).GetComponent<Text> ().text = "Dectivated";
		}
	}


	public void OnVlaueChangeScaling(){

		if (gameObject.GetComponent<Toggle> ().isOn == true) {
			int index = GameController.instance.objnames.IndexOf ("" + gameObject.transform.parent.GetChild (0).GetComponent<Text> ().text);
			GameController.instance.scaleObject.Add (GameController.instance.objfiles [index]);
			GameController.instance.objfiles [index].AddComponent<LeanScale> ();
			gameObject.transform.GetChild (1).GetComponent<Text> ().text = "Activated";
		} else {

			int index = GameController.instance.objnames.IndexOf ("" + gameObject.transform.parent.GetChild (0).GetComponent<Text> ().text);
			GameController.instance.scaleObject.Remove (GameController.instance.objfiles [index]);
			Destroy (GameController.instance.objfiles [index].GetComponent<LeanScale> ());
			gameObject.transform.GetChild (1).GetComponent<Text> ().text = "Dectivated";
		}


	}

}
